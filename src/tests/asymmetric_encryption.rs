use anyhow::{
    ensure,
    bail,
    Context,
};

use sequoia_openpgp as openpgp;
use openpgp::cert::prelude::*;
use openpgp::types::*;
use openpgp::parse::Parse;
use openpgp::serialize::SerializeInto;

use crate::{
    Data,
    Result,
    data,
    plan::Runnable,
    sop::{Sop, Verification},
    tests::TestPlan,
    tests::{
        Expectation,
        TestMatrix,
        RoundtripTest,
        CheckError,
    },
};

mod recipient_ids;
mod corner_cases;
mod rsa_key_sizes;

/// Roundtrip tests check whether consume(produce(x)) yields x.
pub struct EncryptDecryptRoundtrip {
    title: String,
    description: String,
    certs: Vec<Vec<u8>>,
    key: Vec<u8>,
    cipher: Option<openpgp::types::SymmetricAlgorithm>,
    aead: Option<openpgp::types::AEADAlgorithm>,
    message: Data,
    default_expectation: Option<std::result::Result<String, String>>,
}

impl EncryptDecryptRoundtrip {
    pub fn new(title: &str, description: &str, cert: openpgp::Cert,
               message: Data) -> Result<EncryptDecryptRoundtrip> {
        Self::with_certs_and_key(title, description,
                                vec![cert.armored().to_vec()?],
                                cert.as_tsk().armored().to_vec()?,
                                message)
    }

    pub fn with_certs_and_key(title: &str, description: &str,
                             certs: Vec<Vec<u8>>, key: Vec<u8>,
                             message: Data) -> Result<EncryptDecryptRoundtrip> {
        Ok(EncryptDecryptRoundtrip {
            title: title.into(),
            description: description.into(),
            certs,
            key,
            cipher: None,
            aead: None,
            message,
            default_expectation: Some(Ok("Interoperability concern.".into())),
        })
    }

    pub fn with_cipher(title: &str, description: &str, cert: openpgp::Cert,
                       message: Data,
                       cipher: openpgp::types::SymmetricAlgorithm,
                       aead: Option<openpgp::types::AEADAlgorithm>)
                       -> Result<EncryptDecryptRoundtrip>
    {
        // Change the cipher preferences of CERT.
        let uid = cert.with_policy(super::P, None).unwrap()
            .primary_userid().unwrap();
        let mut builder = openpgp::packet::signature::SignatureBuilder::from(
            uid.binding_signature().clone())
            .set_signature_creation_time(Timestamp::now())?
            .set_preferred_symmetric_algorithms(vec![cipher])?;
        if let Some(algo) = aead {
            builder = builder.set_preferred_aead_algorithms(vec![algo])?;
            builder = builder.set_features(
                Features::empty().set_mdc().set_aead())?;
        }
        let mut primary_keypair =
            cert.primary_key()
            .key().clone().parts_into_secret()?.into_keypair()?;
        let new_sig = uid.bind(&mut primary_keypair, &cert, builder)?;
        let cert = cert.insert_packets(Some(new_sig))?;
        let key = cert.as_tsk().armored().to_vec()?;
        let cert = cert.armored().to_vec()?;

        Ok(EncryptDecryptRoundtrip {
            title: title.into(),
            description: description.into(),
            certs: vec![cert],
            key,
            cipher: Some(cipher),
            aead,
            message,
            default_expectation: Some(Ok("Interoperability concern.".into())),
        })
    }
}

impl crate::plan::Runnable<TestMatrix> for EncryptDecryptRoundtrip {
    fn title(&self) -> String {
        self.title.clone()
    }

    fn description(&self) -> String {
        self.description.clone()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        let mut artifacts: Vec<(String, Data)> = self.certs.iter().map(|cert| {
            ("Certificate".into(), cert.clone().into())
        }).collect();
        artifacts.push(
            ("Key".into(), self.key.clone().into())
        );
        artifacts
    }

    fn run(&self, implementations: &[crate::Sop]) -> Result<TestMatrix> {
        RoundtripTest::run(self, implementations)
    }
}

impl RoundtripTest<Data, (Data, Vec<Verification>)> for EncryptDecryptRoundtrip
{
    fn produce(&self, pgp: &Sop)
               -> Result<Data> {
        let mut encrypt = Sop::encrypt(pgp.sop());
        for cert in &self.certs {
            encrypt = encrypt.cert(&cert);
        }
        encrypt.plaintext(&self.message)
    }

    fn check_producer(&self, artifact: &Data) -> Result<()> {
        let pp = openpgp::PacketPile::from_bytes(&artifact)
            .context("Produced data is malformed")?;

        // Check that the number of PKESK packets matches the number of
        // certificates.
        let pkesk_count = pp.children()
            .filter(|p| p.tag() == openpgp::packet::Tag::PKESK)
            .count();
        ensure!(pkesk_count == self.certs.len(),
            CheckError::HardFailure(format!(
                "Expected {:?} PKESK packets, found {:?}",
                self.certs.len(), pkesk_count)));

        // Check whether the implementation used the algorithm(s) that
        // were requested, if any. Note that using a different algorithm
        // is only a "soft failure", as the artifact may still be valid
        // (but using a different algorithm), and the test expectations
        // indicate whether the algorithm is expected to be implemented.

        if let Some(aead_algo) = self.aead {
            match pp.children().last() {
                Some(openpgp::Packet::AED(a)) => {
                    ensure!(a.aead() == aead_algo,
                        CheckError::SoftFailure(format!(
                            "Producer did not use {:?}, but {:?}",
                            aead_algo, a.aead())));

                    if let Some(cipher) = self.cipher {
                        ensure!(a.symmetric_algo() == cipher,
                            CheckError::SoftFailure(format!(
                                "Producer did not use {:?} but {:?}",
                                cipher, a.symmetric_algo())));
                    }
                },
                Some(p) => bail!(CheckError::SoftFailure(format!(
                    "Producer did not use AEAD, found {} packet", p.tag()))),
                None => bail!(CheckError::HardFailure("No packet emitted".into())),
            }
        } else if let Some(cipher) = self.cipher {
            // Check that the producer used CIPHER.
            let cert = openpgp::Cert::from_bytes(&self.key)?;
            let mode = KeyFlags::empty()
                .set_storage_encryption().set_transport_encryption();

            let mut ok = false;
            let mut algos = Vec::new();
            'search: for p in pp.children() {
                if let openpgp::Packet::PKESK(p) = p {
                    for ka in cert.keys().with_policy(super::P, None).secret()
                        .key_flags(mode.clone())
                    {
                        let mut keypair = ka.key().clone().into_keypair()?;
                        if let Some((a, _)) = p.decrypt(&mut keypair, None) {
                            if a == cipher {
                                ok = true;
                                break 'search;
                            }
                            algos.push(a);
                        }
                    }
                }
            }

            ensure!(ok,
                CheckError::SoftFailure(format!(
                    "Producer did not use {:?}, but {:?}", cipher, algos)));
        }

        Ok(())
    }

    fn consume(&self,
               _producer: &Sop,
               consumer: &Sop,
               artifact: &Data)
               -> Result<(Data, Vec<Verification>)> {
        consumer.sop()
            .decrypt()
            .key(&self.key)
            .ciphertext(artifact)
    }

    fn check_consumer(&self, _ciphertext: &Data,
                      (plaintext, _sigs): &(Data, Vec<Verification>))
                      -> Result<()> {
        ensure!(&plaintext[..] == &self.message[..],
            CheckError::HardFailure(format!("Expected {:?}, got {:?}",
                                            self.message, plaintext)));
        Ok(())
    }

    fn expectation(&self, _artifact: Option<&Data>) -> Option<Expectation> {
        if let Some(_) = self.aead {
            // Don't require implementing AEAD for now.
            return None;
        }

        if let Some(cipher) = self.cipher {
            use SymmetricAlgorithm::*;
            match cipher {
                IDEA | TripleDES | CAST5 => // Deprecated in the crypto refresh.
                    Some(Err("Algorithm should be avoided.".into())),
                AES128 =>
                    Some(Ok("Implementations MUST implement AES-128 according to the crypto refresh.".into())),
                AES256 =>
                    Some(Ok("Implementations SHOULD implement AES-256 according to the crypto refresh.".into())),
                _ =>
                    None, // Don't judge.
            }
        } else {
            self.default_expectation.clone()
        }
    }
}

pub fn schedule(plan: &mut TestPlan) -> Result<()> {
    plan.add_section("Asymmetric Encryption");
    plan.add(
        EncryptDecryptRoundtrip::with_certs_and_key(
            "Encrypt-Decrypt roundtrip with minimal key from RFC9760",
            "Encrypt-Decrypt roundtrip with minimal key from \
             Appendix A.3 of RFC9760.",
            vec![data::file("v6/test-vectors/v6-minimal-cert.key").unwrap().into()],
            data::file("v6/test-vectors/v6-minimal-secret.key").unwrap().into(),
            crate::tests::MESSAGE.to_vec().into())?
            .with_tags(&["v6"]));
    plan.add(
        EncryptDecryptRoundtrip::with_certs_and_key(
            "Encrypt with key from RFC9760 and key 'Alice', decrypt with key from RFC9760",
            "Encrypt-Decrypt roundtrip with multiple keys: \
                the message is encrypted with the minimal key from \
                Appendix A.3 of RFC9760 and the 'Alice' key from \
                draft-bre-openpgp-samples-00; and decrypted using \
                the key from RFC9760 only.",
            vec![data::file("v6/test-vectors/v6-minimal-cert.key").unwrap().into(), data::certificate("alice.pgp").into()],
            data::file("v6/test-vectors/v6-minimal-secret.key").unwrap().into(),
            crate::tests::MESSAGE.to_vec().into())?
            .with_tags(&["v6"]));
    plan.add(
        EncryptDecryptRoundtrip::with_certs_and_key(
            "Encrypt with key from RFC9760 and key 'Alice', decrypt with key 'Alice'",
            "Encrypt-Decrypt roundtrip with multiple keys: \
                the message is encrypted with the minimal key from \
                Appendix A.3 of RFC9760 and the 'Alice' key from \
                draft-bre-openpgp-samples-00; and decrypted using \
                the 'Alice' key only.",
            vec![data::file("v6/test-vectors/v6-minimal-cert.key").unwrap().into(), data::certificate("alice.pgp").into()],
            data::certificate("alice-secret.pgp").into(),
            crate::tests::MESSAGE.to_vec().into())?
            .with_tags(&["v6"]));
    plan.add(Box::new(
        EncryptDecryptRoundtrip::new(
            "Encrypt-Decrypt roundtrip with key 'Alice'",
            "Encrypt-Decrypt roundtrip using the 'Alice' key from \
             draft-bre-openpgp-samples-00.",
            openpgp::Cert::from_bytes(data::certificate("alice-secret.pgp"))?,
            crate::tests::MESSAGE.to_vec().into())?));
    plan.add(Box::new(
        EncryptDecryptRoundtrip::new(
            "Encrypt-Decrypt roundtrip with key 'Bob'",
            "Encrypt-Decrypt roundtrip using the 'Bob' key from \
             draft-bre-openpgp-samples-00.",
            openpgp::Cert::from_bytes(data::certificate("bob-secret.pgp"))?,
            crate::tests::MESSAGE.to_vec().into())?));
    plan.add(Box::new(
        EncryptDecryptRoundtrip {
            title: "Encrypt-Decrypt roundtrip with key 'Carol'".into(),
            description: "Encrypt-Decrypt roundtrip using the 'Carol' key from \
             draft-bre-openpgp-samples-00.".into(),
            certs: vec![data::certificate("carol.pgp").into()],
            key: data::certificate("carol-secret.pgp").into(),
            cipher: None,
            aead: None,
            message: crate::tests::MESSAGE.to_vec().into(),
            default_expectation: None,
        }));
    plan.add(
        EncryptDecryptRoundtrip {
            title: "Encrypt-Decrypt roundtrip with key 'John'".into(),
            description:
            "Encrypt-Decrypt roundtrip using the v3 'John' key.".into(),
            certs: vec![data::certificate("john.pgp").into()],
            key: data::certificate("john-secret.pgp").into(),
            cipher: None,
            aead: None,
            message: crate::tests::MESSAGE.to_vec().into(),
            default_expectation: None,
        }
        .with_tags(&["v3"]));

    plan.add(Box::new(recipient_ids::RecipientIDs::new()?));
    plan.add(Box::new(corner_cases::RSAEncryption::new()));
    plan.add(Box::new(rsa_key_sizes::RSAKeySizes::new()?));
    Ok(())
}
