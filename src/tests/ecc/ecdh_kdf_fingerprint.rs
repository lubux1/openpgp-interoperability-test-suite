use anyhow::ensure;

use crate::{
    Data,
    Result,
    data,
    sop::{Sop, Verification},
    tests::{
        Expectation,
        TestMatrix,
        ConsumptionTest,
        CheckError,
    },
};

/// Tests whether implementations are willing to unwrap session keys
/// using the recipient's primary key fingerprint.
pub struct ECDHKDFFingerprint {
}

impl ECDHKDFFingerprint {
    pub fn new() -> Result<ECDHKDFFingerprint> {
        Ok(ECDHKDFFingerprint {})
    }
}

impl crate::plan::Runnable<TestMatrix> for ECDHKDFFingerprint {
    fn title(&self) -> String {
        "ECDH KDF using recipient fingerprint".into()
    }

    fn description(&self) -> String {
        "Tests whether implementations are willing to unwrap session keys \
         using the recipient's primary key fingerprint.".into()
    }

    fn artifacts(&self) -> Vec<(String, Data)> {
        vec![
            ("Key".into(), data::certificate("alice-secret.pgp").into()),
        ]
    }

    fn run(&self, implementations: &[crate::Sop])
           -> Result<TestMatrix> {
        ConsumptionTest::run(self, implementations)
    }
}

impl ConsumptionTest<Data, (Data, Vec<Verification>)> for ECDHKDFFingerprint {
    fn produce(&self) -> Result<Vec<(String, Data, Option<Expectation>)>> {
        // The test cases are produced with a hacked up version of
        // sequoia-openpgp 1.9.
        Ok(vec![
            ("KDF the subkey's fingerprint".into(),
             data::message("alice-kdf-subkey.pgp").into(),
             Some(Ok("Base case".into()))),
            ("KDF the primary key's fingerprint".into(),
             data::message("alice-kdf-primary.pgp").into(),
             None),
        ])
    }

    fn consume(&self, pgp: &Sop, artifact: &Data)
               -> Result<(Data, Vec<Verification>)> {
        pgp.sop().decrypt()
            .key(data::certificate("alice-secret.pgp"))
            .ciphertext(artifact)
    }

    fn check_consumer(&self, _artifact: &Data,
                      (plaintext, _sigs): &(Data, Vec<Verification>),
                      _expectation: &Option<Expectation>)
                      -> Result<()> {
        ensure!(&plaintext[..] == crate::tests::MESSAGE,
            CheckError::HardFailure(format!(
                "Expected {:?}, got {:?}",
                crate::tests::MESSAGE, plaintext)));
        Ok(())
    }
}
