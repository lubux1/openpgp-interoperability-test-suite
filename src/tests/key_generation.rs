use std::collections::HashSet;

use anyhow::ensure;

use sequoia_openpgp as openpgp;
use openpgp::parse::Parse;

use crate::{
    Data,
    Result,
    sop::{Sop, Verification},
    tests::TestPlan,
    tests::{
        TestMatrix,
        RoundtripTest,
        CheckError,
    },
};

pub struct GenerateThenEncryptDecryptRoundtrip {
    title: String,
    userids: HashSet<String>,
}

impl GenerateThenEncryptDecryptRoundtrip {
    pub fn new(title: &str, userids: &[&str])
               -> GenerateThenEncryptDecryptRoundtrip {
        GenerateThenEncryptDecryptRoundtrip {
            title: title.into(),
            userids: userids.iter().map(|u| u.to_string()).collect(),
        }
    }
}

impl crate::plan::Runnable<TestMatrix> for GenerateThenEncryptDecryptRoundtrip {
    fn title(&self) -> String {
        self.title.clone()
    }

    fn description(&self) -> String {
        "This models key generation, distribution, and encrypted
        message exchange.  Generates a default key with the producer
        <i>P</i>, then extracts the certificate from the key and uses
        it to encrypt a message using the consumer <i>C</i>, and
        finally <i>P</i> to decrypt the message.".into()
    }

    fn tags(&self) -> std::collections::BTreeSet<&'static str> {
        ["v6"].iter().cloned().collect()
    }

    fn run(&self, implementations: &[crate::Sop]) -> Result<TestMatrix> {
        RoundtripTest::run(self, implementations)
    }
}

impl RoundtripTest<(Data, Data), (Data, Vec<Verification>)>
    for GenerateThenEncryptDecryptRoundtrip
{
    fn produce(&self, _: &Sop) -> Result<(Data, Data)> {
        unreachable!("Self::produce_artifacts is used instead")
    }

    fn produce_artifacts(&self, pgp: &Sop)
                         -> Result<Vec<(Option<String>, Result<(Data, Data)>)>>
    {
        let userids = self.userids.iter().map(|s| &s[..]).collect::<Vec<_>>();

        let extract_cert = |key: Data| -> Result<(Data, Data)> {
            let cert = pgp.sop()
                .extract_cert()
                .key(&key)?;
            Ok((key, cert))
        };

        if let Some(profiles) = pgp.sop()
            .version()?.profiles.get("generate-key")
        {
            Ok(profiles.iter().map(
                |p| (Some(p.0.clone()),
                     pgp.sop()
                     .generate_key()
                     .profile(&p.0)
                     .userids(userids.iter().cloned())
                     .and_then(extract_cert)))
               .collect())
        } else {
            Ok(vec![(None,
                     pgp.sop()
                     .generate_key()
                     .userids(userids.iter().cloned())
                     .and_then(extract_cert))])
        }
    }

    fn check_producer(&self, (artifact, _): &(Data, Data)) -> Result<()> {
        let tpk = match openpgp::Cert::from_bytes(&artifact) {
            Ok(c) => c,
            Err(e) => if let Some(openpgp::Error::UnsupportedCert2(_, _))
                = e.downcast_ref::<openpgp::Error>() {
                    // We don't understand the certificate, skip
                    // sanity checks.
                    return Ok(());
                } else {
                    return Err(e);
                },
        };
        let userids: HashSet<String> = tpk.userids()
            .map(|uidb| {
                String::from_utf8_lossy(uidb.userid().value()).to_string()
            })
            .collect();

        let missing: Vec<&str> = self.userids.difference(&userids)
            .map(|s| &s[..]).collect();
        ensure!(missing.is_empty(),
            CheckError::HardFailure(format!("Missing userids: {:?}",
                                            missing)));

        let additional: Vec<&str> = userids.difference(&self.userids)
            .map(|s| &s[..]).collect();
        ensure!(additional.is_empty(),
            CheckError::HardFailure(format!("Additional userids: {:?}",
                                            additional)));

        Ok(())
    }

    fn consume(&self,
               producer: &Sop,
               consumer: &Sop,
               (key, cert): &(Data, Data))
               -> Result<(Data, Vec<Verification>)> {
        let ciphertext = consumer.sop()
            .encrypt()
            .cert(cert)
            .plaintext(crate::tests::MESSAGE)?;
        producer.sop()
            .decrypt()
            .key(key)
            .ciphertext(&ciphertext)
    }
}

pub fn schedule(plan: &mut TestPlan) -> Result<()> {
    plan.add_section("Key Generation");

    plan.add(Box::new(
        GenerateThenEncryptDecryptRoundtrip::new(
            "Default key generation, encrypt-decrypt roundtrip",
            &["Bernadette <b@example.org>"])));
    plan.add(Box::new(
        GenerateThenEncryptDecryptRoundtrip::new(
            "Default key generation, encrypt-decrypt roundtrip, 2 UIDs",
            &["Bernadette <b@example.org>", "Soo <s@example.org>"])));
    plan.add(Box::new(
        GenerateThenEncryptDecryptRoundtrip::new(
            "Default key generation, encrypt-decrypt roundtrip, no UIDs",
            &[])));
    Ok(())
}
