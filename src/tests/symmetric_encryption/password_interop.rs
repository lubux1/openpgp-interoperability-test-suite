use anyhow::ensure;

use crate::{
    Data,
    Result,
    sop::{Sop, Verification},
    tests::{
        Expectation,
        TestMatrix,
        RoundtripTest,
        CheckError,
    },
};

/// Roundtrip tests check whether consume(produce(x)) yields x.
pub struct PasswordEncryptionInterop {
}

impl PasswordEncryptionInterop {
    pub fn new() -> Result<PasswordEncryptionInterop> {
        Ok(PasswordEncryptionInterop {})
    }
}

impl crate::plan::Runnable<TestMatrix> for PasswordEncryptionInterop {
    fn title(&self) -> String {
        "Password-based encryption".into()
    }

    fn description(&self) -> String {
        format!("Encrypts a message using the password {:?}, \
                 and tries to decrypt it.", crate::tests::PASSWORD)
    }

    fn run(&self, implementations: &[crate::Sop]) -> Result<TestMatrix> {
        RoundtripTest::run(self, implementations)
    }
}

impl RoundtripTest<Data, (Data, Vec<Verification>)> for PasswordEncryptionInterop
{
    fn produce(&self, pgp: &Sop)
               -> Result<Data> {
        pgp.sop().encrypt()
            .with_password(crate::tests::PASSWORD)
            .plaintext(crate::tests::MESSAGE)
    }

    fn consume(&self,
               _producer: &Sop,
               consumer: &Sop,
               artifact: &Data)
               -> Result<(Data, Vec<Verification>)> {
        consumer.sop()
            .decrypt()
            .with_password(crate::tests::PASSWORD)
            .ciphertext(artifact)
    }

    fn check_consumer(&self, _artifact: &Data,
                      (plaintext, _sigs): &(Data, Vec<Verification>))
                      -> Result<()> {
        ensure!(&plaintext[..] == crate::tests::MESSAGE,
            CheckError::HardFailure(format!("Expected {:?}, got {:?}",
                                            crate::tests::MESSAGE, plaintext)));
        Ok(())
    }

    fn expectation(&self, _artifact: Option<&Data>) -> Option<Expectation> {
        Some(Ok("Interoperability concern.".into()))
    }
}
