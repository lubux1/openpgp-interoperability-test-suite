#+TITLE: Interop Testing the Crypto Refresh
#+AUTHOR: Justus Winter <justus@sequoia-pgp.org>
#+DATE: IETF 112, 2021-11-10
#+OPTIONS: H:1 toc:nil num:t
#+OPTIONS: tex:t
#+startup: beamer
#+LATEX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [presentation]
#+LATEX_HEADER: \usepackage[normalem]{ulem}
#+LATEX_HEADER: \usepackage{pifont}
#+LATEX_HEADER: \usepackage[export]{adjustbox}
#+LATEX_HEADER: \newcommand{\cmark}{\ding{51}}
#+LATEX_HEADER: \newcommand{\xmark}{\ding{55}}
#+LaTeX_HEADER: \hypersetup{linktoc = all, colorlinks = true}
#+BEAMER_HEADER: \title[Interop Testing the Crypto Refresh]{Interop Testing the Crypto Refresh}
#+BEAMER_HEADER: \titlegraphic{\url{https://tests.sequoia-pgp.org}\\ \vspace{3mm} \url{https://sequoia-pgp.org/talks/2021-11-ietf/interop-testing-the-crypto-refresh.pdf}}
#+BEAMER_THEME: Madrid
#+COLUMNS: %45ITEM %10BEAMER_ENV(Env) %10BEAMER_ACT(Act) %4BEAMER_COL(Col) %8BEAMER_OPT(Opt)

* A little Context, please?
** ...                                                                :BMCOL:
   :PROPERTIES:
   :BEAMER_col: 0.5
   :END:

  - circa 90 tests
  - around 800 test vectors
  - found at least 92 bugs in 10 implementations

** ...                                                                :BMCOL:
   :PROPERTIES:
   :BEAMER_col: 0.5
   :END:

  - improved implementations
  - improved our understanding of the ecosystem
  - highlights areas where implementations lack guidance

* The How?
** A block                                                            :BMCOL:
   :PROPERTIES:
   :BEAMER_col: 0.4
   :END:

  - black box
    - consumer tests
    - producer-consumer tests

  - common interface
    - [[https://tools.ietf.org/html/draft-dkg-openpgp-stateless-cli-03][Stateless OpenPGP interface]]

\vspace{1cm}
\small
$ sqop generate-key >key

$ sqop sign key <msg >sig

$ rpmsop verify sig key <msg

** A screenshot                                                       :BMCOL:
   :PROPERTIES:
   :BEAMER_col: 0.6
   :END:

\center\includegraphics[width=\linewidth]{./sample-test.pdf}

* What's next?

  - test features from the crypto refresh while we refine it

    - most artifacts created by Sequoia
    - but can be shipped as files

  - publish results with a focus on the crypto refresh

    - tag relevant tests
    - target development versions

  - \to well-understood and implementable spec

  - \to speed-up adoption

* What can you do?

  - standardization enthusiast

    - help curate a list of useful tests
    - write tests
    - review tests and results

  - implementor

    - tell me how to get/build/update your (SOP) implementation
    - consider implementing your own SOP frontend
      - if you do, clue me how to keep it up-to-date
    - get in touch, be responsive

* Join the Fun?
** A block                                                            :BMCOL:
   :PROPERTIES:
   :BEAMER_col: 0.4
   :END:

  - add tests
    - talk to me
    - open an [[https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite/-/issues][issue]]
  - add an implementation
    - AWESOME!
    - implement the [[https://tools.ietf.org/html/draft-dkg-openpgp-stateless-cli-03][Stateless OpenPGP interface]]
    - talk to me
  - argue semantics
    - talk to me
    - open an [[https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite/-/issues][issue]]
    - discuss on openpgp@ietf.org

** run the test suite                                                 :BMCOL:
   :PROPERTIES:
   :BEAMER_env: block
   :BEAMER_col: 0.6
   :END:

   $ git clone https://gitlab.com/sequoia-pgp/openpgp-interoperability-test-suite

   $ less README.md # optional; YOLO

   $ apt install sqop # optional

   $ cp config.json.dist config.json

   $ editor config.json

   $ cargo run -\phantom{}-

   \hspace{1cm} -\phantom{}-retain-tests keyword

   \hspace{1cm} -\phantom{}-html-out results.html

   \hspace{1cm} -\phantom{}-json-out results.json

